package com.cognizant.iot.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.multidex.MultiDexApplication;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.cognizant.iot.helper.LocaleHelper;
import com.cognizant.iot.receiver.ConnectivityReceiver;

public class GlobalClass extends MultiDexApplication {


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"));
    }

    String mutex = "empty";
    public static final String TAG = GlobalClass.class
            .getSimpleName();
    static Boolean NetSuite = true;
    static Boolean AX = false;

    double lpRedeemed = 0;
    boolean lp;

    private RequestQueue mRequestQueue;
    private static Context sContext;
    private static GlobalClass mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        GlobalClass.setAppContext(getApplicationContext());
        mInstance = this;
    }

    public static void setAppContext(Context context) {
        Log.d(TAG, String.format("setAppContext: %s", context));
        sContext = context;
    }

    public static Context getAppContext() {
        return sContext;
    }

    public static synchronized GlobalClass getInstance() {
        return mInstance;
    }


    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }


    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public String getServer() {

        if (NetSuite) {
            return "http://c3a2351402de471d85e8257dd5d2f031.cloudapp.net";
        } else if (AX) {
            return "http://54d2617d682144168816083a087d52ed.cloudapp.net";
        }
        //cloudax.dynamics
        return "";

    }

    public static boolean checkPermission(Activity activity, String permission) {
        int result = ContextCompat.checkSelfPermission(activity, permission);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public String getMutex() {
        return mutex;
    }

    public void setMutex(String mutex) {
        this.mutex = mutex;
    }

    public void setNetSuite(Boolean netSuite) {
        NetSuite = netSuite;
    }

    public void setAX(Boolean aX) {
        AX = aX;
    }

    public Boolean getNetSuite() {
        return NetSuite;
    }

    public Boolean getAX() {
        return AX;
    }

    public double getLpRedeemed() {
        return lpRedeemed;
    }

    public void setLpRedeemed(double lpRedeemed) {
        this.lpRedeemed = lpRedeemed;
    }

    public boolean isLp() {
        return lp;
    }

    public void setLp(boolean lp) {
        this.lp = lp;
    }

}
