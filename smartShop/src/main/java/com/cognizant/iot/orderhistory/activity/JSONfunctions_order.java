package com.cognizant.iot.orderhistory.activity;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.MySSLSocketFactory;

import android.util.Log;

public class JSONfunctions_order {

	public static JSONObject getJSONfromAZUREAXURLACESSTOKEN(String url) {
		InputStream is = null;
		String result = "";
		JSONObject jArray = null;
		ArrayList<NameValuePair> postParameters;

		// Download JSON data from URL
		try {
			// HttpClient httpclient = new DefaultHttpClient();

			HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
			HttpPost httppost = new HttpPost(url);

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("UserName",
					"saddaf@tanzeemcognizant.onmicrosoft.com"));
			params.add(new BasicNameValuePair("Pwd", "Ttoolshe1,.2"));

			UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,
					HTTP.UTF_8);
			httppost.setEntity(ent);

			HttpResponse response = httpclient.execute(httppost);
			HttpEntity httpentity = response.getEntity();
			is = httpentity.getContent();

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// Convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		try {

			jArray = new JSONObject(result);
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}

		return jArray;
	}

	public static JSONObject getJSONfromAXURLACESSTOKEN(String url) {
		InputStream is = null;
		String result = "";
		JSONObject jArray = null;
		ArrayList<NameValuePair> postParameters;

		// Download JSON data from URL
		try {
			// HttpClient httpclient = new DefaultHttpClient();

			HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
			HttpPost httppost = new HttpPost(url);

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("grant_type", "password"));
			params.add(new BasicNameValuePair("client_id", "Cloud POS"));
			params.add(new BasicNameValuePair("username", "000095"));
			params.add(new BasicNameValuePair("password", "123456"));

			UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,
					HTTP.UTF_8);
			httppost.setEntity(ent);

			HttpResponse response = httpclient.execute(httppost);
			HttpEntity httpentity = response.getEntity();
			is = httpentity.getContent();

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// Convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		try {

			jArray = new JSONObject(result);
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}

		return jArray;
	}


	/*
			For getting cart details from AX7 latest
	 */
	public static JSONObject getJSONfromAXURL(String url) {
		InputStream is = null;
		String result = "";
		JSONObject jArray = null;
		ArrayList<NameValuePair> postParameters;

		// Download JSON data from URL
		try {


			HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
			HttpPost httppost = new HttpPost(url);

			HttpResponse response = httpclient.execute(httppost);
			HttpEntity httpentity = response.getEntity();
			is = httpentity.getContent();

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// Convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		try {

			jArray = new JSONObject(result);
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}

		return jArray;
	}

	public static JSONObject getJSONfromURL(String url) {
		InputStream is = null;
		String result = "";
		JSONObject jArray = null;
		ArrayList<NameValuePair> postParameters;

		// Download JSON data from URL
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("UserID", AccountState.getUserID()));

			UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,
					HTTP.UTF_8);
			httppost.setEntity(ent);


			HttpResponse response = httpclient.execute(httppost);
			HttpEntity httpentity = response.getEntity();
			is = httpentity.getContent();

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// Convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		try {

			jArray = new JSONObject(result);
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}

		return jArray;
	}

	public static JSONObject getJSONfromURL_order_products(String url,
			String orderid) {
		InputStream is = null;
		String result = "";
		JSONObject jArray = null;

		// Download JSON data from URL
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("macaddr", Order_Screen.imei));
			params.add(new BasicNameValuePair("order", orderid));
			// params.add(new BasicNameValuePair("pass", "xyz"));
			UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,
					HTTP.UTF_8);
			httppost.setEntity(ent);

			HttpResponse response = httpclient.execute(httppost);
			HttpEntity httpentity = response.getEntity();
			is = httpentity.getContent();

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// Convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		try {

			jArray = new JSONObject(result);
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}

		return jArray;
	}

}
