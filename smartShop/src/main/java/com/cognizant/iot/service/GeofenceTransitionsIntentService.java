package com.cognizant.iot.service;

/**
 * Created by 599654 on 1/17/2017.
 */

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;


import com.cognizant.iot.geolocation.Geofence_MainActivity;
import com.cognizant.retailmate.R;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

public class GeofenceTransitionsIntentService extends IntentService {

    private static final String TAG = "GeofenceTransitions";

    public GeofenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.d("API", "GeofenceIntent Triggered");

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            Log.e(TAG, "Goefencing Error " + geofencingEvent.getErrorCode());
            return;
        } else {

            // Get the transition type.
            int geofenceTransition = geofencingEvent.getGeofenceTransition();

            List<Geofence> geofences = geofencingEvent.getTriggeringGeofences();

            //Get all triggering GEOFENCES  -usually used when geofences are overlapping
//            Geofence geofence = geofences.get(0);             /* Alternative when only one is triggered */
            for (Geofence i : geofences) {
                String requestId = i.getRequestId();
                Log.d("API Test", requestId);

                //Check for ENTRY trigger. EXIT and DWELL can be used if needed
                if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
                    showToast("Entered "+requestId);
                    Log.d("API", requestId + " The user has Entered");
//                    showNotification("Entered", requestId + "Entered the Location");
                } else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
                    showToast("Exited "+requestId);
                    Log.d("API", requestId + " The user has Exited");
//                    showNotification("Exited",requestId+"Exited the Location");
                } else {
                    showToast("Error");
//                    showNotification("Error", "Error");
                }
            }
        }
    }

    public void showToast(final String s) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(getApplicationContext(),s, Toast.LENGTH_LONG).show();
            }
        });

    }


    //TO SHOW NOTIFICATION ON NOTIFICATION BAR
    public void showNotification(String text, String bigText) {

        // 1. Create a NotificationManager
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        // 2. Create a PendingIntent for AllGeofencesActivity
        Intent intent = new Intent(this, Geofence_MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // 3. Create and send a notification
        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.retail_mate_app_icon)
                .setContentTitle(text)
                .setContentText(text)
                .setContentIntent(pendingNotificationIntent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(bigText))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .build();
        notificationManager.notify(0, notification);
    }
}