package com.cognizant.iot.activity;

import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.retailmate.R;

/*ProductAdapter class is designed to be attached to a list view, 
 and populate the data for the different products that will be in a list view
 It uses the Item layout to populate the data in the list view.
 */

public class ProductAdapter_catalog extends BaseAdapter {
	private List<Product> mProductList;
	private LayoutInflater mInflater;
	// private boolean mShowPrice;
	private boolean mShowName;

	// constructor of this class
	public ProductAdapter_catalog(List<Product> list, LayoutInflater inflater,
			boolean showName, boolean showPrice) {
		mProductList = list;
		mInflater = inflater;
		mShowName = showName;
		// mShowPrice = showPrice;

	}

	@Override
	public int getCount() {
		return mProductList.size();
	}

	@Override
	public Object getItem(int position) {
		return mProductList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final ViewItem item;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_catalog, null); // gets
																			// the
																			// view
																			// of
																			// the
																			// item.xml
																			// layout

			item = new ViewItem();

			item.productImageView = (ImageView) convertView // gets the image
					.findViewById(R.id.productimage);
			item.productTitle = (TextView) convertView
					.findViewById(R.id.product_title); // gets the textView
			/*
			 * item.productPrice = (TextView) convertView
			 * .findViewById(R.id.product_price);
			 */// gets the price

			convertView.setTag(item);

		} else {
			item = (ViewItem) convertView.getTag(); // if something is already
													// there
		}

		Product curProduct = mProductList.get(position);

		item.productImageView.setImageDrawable(curProduct.productImage);
		item.productTitle.setText(curProduct.title);
		// String pr= String.valueOf( curProduct.price);
		// item.productPrice.setText(pr);

		if (!mShowName) // make the name text box invisible
			item.productTitle.setVisibility(View.GONE);
		/*
		 * if(!mShowPrice) //make the price text box invisible
		 * item.productPrice.setVisibility(View.GONE);
		 */

		return convertView;
	}

	private class ViewItem {
		ImageView productImageView;
		TextView productTitle;
		// TextView productPrice;
	}
}