package com.cognizant.iot.insidestore;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cognizant.retailmate.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by 452781 on 12/6/2016.
 */

public class InsideStoreOffersAdapter extends RecyclerView.Adapter<InsideStoreOffersAdapter.ProductHolder> {
    private static final String CustomTag = "CustomTag";

    private ArrayList<InsideStoreOffersModel> offersList;
    private Context mContext;

    public InsideStoreOffersAdapter(Context context, ArrayList<InsideStoreOffersModel> offersList) {
        this.offersList = offersList;
        this.mContext = context;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.insidestore_offers_item, null);
        ProductHolder mh = new ProductHolder(v);
        return mh;
    }


    @Override
    public void onBindViewHolder(ProductHolder holder, final int i) {

        InsideStoreOffersModel offerData = offersList.get(i);

        holder.promonameTitle.setText(offerData.getPROMODESC());
        holder.promoIDView.setText(offerData.getPROMOID());

        if (i % 2 == 0) {
            Picasso.with(mContext)
                    .load(R.drawable.specialoffers_banner)
                    .placeholder(R.drawable.special_offers_banner)
                    .error(R.drawable.special_offers_banner)
                    .into(holder.itemImage);
        } else {
            Picasso.with(mContext)
                    .load(R.drawable.special_offers_banner)
                    .placeholder(R.drawable.specialoffers_banner)
                    .error(R.drawable.specialoffers_banner)
                    .into(holder.itemImage);
        }

//        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d(CustomTag, String.valueOf(ChatGlobal.tapToAdd));
//
//                if (ChatGlobal.tapToAdd) {
//
//                    ChatGlobal.cartBuffer.append(itemsList.get(i).getProductName() + " added to cart.");
//                    Log.d(CustomTag, "cartBuffer = " + ChatGlobal.cartBuffer.toString());
//                    ChatMain.addTocart();
//                    ChatGlobal.productID = itemsList.get(i).getProductId();
//                    Log.e(CustomTag, itemsList.get(i).getProductId() + "********" + itemsList.get(i).getProductName());
//
//                    //Toast.makeText(v.getContext(), tvTitle.getText()+ " added to cart", Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(v.getContext(), itemsList.get(i).getProductName(), Toast.LENGTH_SHORT).show();
//                }
//
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return (null != offersList ? offersList.size() : 0);
    }

    public class ProductHolder extends RecyclerView.ViewHolder {

        protected TextView promonameTitle;

        protected ImageView itemImage;

        TextView promoIDView;

        LinearLayout linearLayout;


        public ProductHolder(final View view) {
            super(view);

            this.promonameTitle = (TextView) view.findViewById(R.id.promoName);
            this.itemImage = (ImageView) view.findViewById(R.id.promoImage);
            this.promoIDView = (TextView) view.findViewById(R.id.promoID);
            this.linearLayout = (LinearLayout) view.findViewById(R.id.promo_single_card_layout);

        }

    }

}