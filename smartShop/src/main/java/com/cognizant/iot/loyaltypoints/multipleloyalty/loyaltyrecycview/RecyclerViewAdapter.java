package com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltyrecycview;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.cognizant.iot.loyaltypoints.multipleloyalty.DetailedLoyaltyActivity;
import com.cognizant.retailmate.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 543898 on 12/26/2016.
 */


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    List<LoyaltyCards> loyaltyCardses = new ArrayList<>();
    Context context1;

    public RecyclerViewAdapter(Context context2, List<LoyaltyCards> values2) {

        loyaltyCardses = values2;

        context1 = context2;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;

        public ImageView imageView;

        public LinearLayout linearLayout;

        public ViewHolder(View v) {

            super(v);

            textView = (TextView) v.findViewById(R.id.textview1);

            imageView = (ImageView) v.findViewById(R.id.brand_img);

            linearLayout = (LinearLayout) v.findViewById(R.id.liner_layout);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view1 = LayoutInflater.from(context1).inflate(R.layout.loyalty_multiple_recycler_view_items, parent, false);

        ViewHolder viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, final int position) {

        Vholder.textView.setText(loyaltyCardses.get(position).getRewards());

        Vholder.imageView.setImageResource(loyaltyCardses.get(position).getRewardsBrand());

        Vholder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    Intent intent = new Intent(context1, DetailedLoyaltyActivity.class);
                    context1.startActivity(intent);
                } else {
                    Toast.makeText(context1, loyaltyCardses.get(position).getRewards(), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {

        return loyaltyCardses.size();
    }
}