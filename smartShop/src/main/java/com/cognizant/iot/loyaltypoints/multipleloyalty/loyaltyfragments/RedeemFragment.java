package com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltyfragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltymodel.LoyaltyResponseModel;
import com.cognizant.retailmate.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by 543898 on 1/23/2017.
 */
public class RedeemFragment extends Fragment {


    Gson gson;

    LoyaltyResponseModel loyaltyResponseModel;

    public RedeemFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gson = new Gson();
        loyaltyResponseModel = gson.fromJson(loadJSONFromAsset(), LoyaltyResponseModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View redemption = inflater.inflate(R.layout.loyalty_multiple_redemption, container, false);

        TextView points, nextLevel;

        points = (TextView) redemption.findViewById(R.id.loyalty_points);

        nextLevel = (TextView) redemption.findViewById(R.id.next_level);

        points.setText(loyaltyResponseModel.getReedemptionPoint());

        nextLevel.setText("Next level at " + ((Integer.parseInt(loyaltyResponseModel.getReedemptionPoint()) + 99) / 100) * 100);

        return redemption;

    }


    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getApplicationContext().getAssets().open("loyaltyoffline.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
