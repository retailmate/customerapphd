package com.cognizant.iot.loyaltypoints.simpleloyalty;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.orderhistory.activity.ImageLoader_Orders;
import com.cognizant.retailmate.R;

public class LoyaltyPointsAdapter extends BaseAdapter {

	// Declare Variables
	Context context;
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	ImageLoader_Orders imageLoader;
	HashMap<String, String> resultp = new HashMap<String, String>();
	HashMap<String, String> resultp2 = new HashMap<String, String>();
	ArrayList<HashMap<String, String>> arraylist;
	JSONObject jsonobject;
	JSONArray jsonarray;

	Boolean har = false;
	ProgressDialog prgDialog;

	String prodid1;

	public LoyaltyPointsAdapter(final Context context,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context;
		data = arraylist;
		imageLoader = new ImageLoader_Orders(context);

		System.out.println("LOYALTYpointsADAPTER arraylist "
				+ data.get(data.size() - 1).get("type"));

	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables
		TextView prodid, serialnumberLoyalty;
		TextView prodname;
		TextView transtypeloyalty;
		ImageView flag;
		TextView datetimeLoyalty;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = inflater.inflate(R.layout.loyalty_listview_row, parent,
				false);
		// Get the position
		resultp = data.get(data.size() - position - 1);

		// Locate the TextViews in listview_item.xml
		// prodid = (TextView) itemView.findViewById(R.id.order_product_prodid);
		serialnumberLoyalty = (TextView) itemView
				.findViewById(R.id.serialnumberLoyalty);
		transtypeloyalty = (TextView) itemView
				.findViewById(R.id.transtypeloyalty);
		datetimeLoyalty = (TextView) itemView
				.findViewById(R.id.datetimeLoyalty);

		// Capture position and set results to the TextViews
		// prodid.setText(resultp.get(Order_Products.PRODID));
		serialnumberLoyalty.setText(String.valueOf(position + 1).toString());
		transtypeloyalty.setText(resultp.get("points") + " points, "
				+ resultp.get("type"));

		String strDateTimeBoj = resultp.get("date");
		// first you need to use proper date formatter
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		try {
			Date date = df.parse(strDateTimeBoj);
			System.out.println(df.format(date));

			datetimeLoyalty.setText((date.getDate() + "/"
					+ (date.getMonth() + 1) + "/" + (date.getYear() + 1900)
					+ " | " + date.getHours() + ":" + date.getMinutes())
					.toString());

			// datetimeLoyalty.setText(date.toString().substring(0,
			// date.toString().indexOf("G")));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}// converting String to date

		// Capture position and set results to the ImageView

		return itemView;
	}

}