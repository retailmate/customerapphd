package com.cognizant.iot.wearable.companion.service;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.WindowManager;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cognizant.iot.activity.Login_Screen;
import com.cognizant.iot.model.AddCustomerModel;
import com.cognizant.iot.model.CustomerGreetingModel;
import com.cognizant.iot.service.CustomerSignalRService;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.wearable.companion.Constants;
import com.cognizant.iot.wearable.companion.model.Alert;
import com.cognizant.iot.wearable.companion.model.Alerts;
import com.cognizant.iot.wearable.companion.model.Offer;
import com.cognizant.retailmate.R;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class BeaconService extends Service {

    Gson gson = new Gson();

    String _iBeaconUUID = null;
    double _proxDistance;
    double _prevProximityDistance = 0;
    // IBeaconManager iBeaconManager;

    /*
     *
     * NEW
     */
    int callCount = 0;
    Boolean customerInBoundState = false;
    private static final String TAG = "BeaconService";
    private BeaconManager beaconManager;
    private Region region;

    CountDownTimer timer;
    private static final double enterThreshold = 10;
    private static final double exitThreshold = 30;
    ArrayList<HashMap<String, Object>> history = new ArrayList<>();
    NotificationManagerCompat notificationManager;

    final static String GROUP_KEY_OFFERS = "group_key_offers";

    @Override
    public void onCreate() {
        Log.d("@@##", "serviceInit");
        logToDisplay("serviceInit");
        notificationManager = NotificationManagerCompat.from(this);
        // serviceInit();
        Log.i(TAG, "Service onCreate");
        beaconManager = new BeaconManager(this);
        Log.i(TAG, "beaconManager = " + beaconManager);

    }

    private void serviceInit() {
        // iBeaconManager = IBeaconManager.getInstanceForApplication(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onDestroy() {
        // iBeaconManager.unBind(this);
        beaconManager.disconnect();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Service onStartCommand");
        beaconManager = new BeaconManager(this);
        startRanging();

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startRanging(region);
            }
        });

        return Service.START_STICKY;
    }

    private void startRanging() {

        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> list) {
                if (!list.isEmpty()) {
                    Beacon nearestBeacon = list.get(0);
                    Log.d("Service for watch ",
                            "Nearest getMacAddress: "
                                    + nearestBeacon.getMacAddress());
                    Log.d("Service for watch ", "Nearest getProximityUUID: "
                            + nearestBeacon.getProximityUUID());

					/*
                     * new
					 */

                    logToDisplay("inside didRangeBeaconsInRegion");
                    _iBeaconUUID = nearestBeacon.getProximityUUID().toString();
                    _proxDistance = (double) Math.round(Utils
                            .computeAccuracy(nearestBeacon) * 100) / 100;
                    // _proxDistance = iBeacons.iterator().next().getAccuracy();
                    // int proximity =
                    // iBeacons.iterator().next().getProximity();
//					Log.d("@@##", "_iBeaconUUID = " + _iBeaconUUID);
                    SharedPreferences pref = getApplicationContext()
                            .getSharedPreferences("MyPref", 0);

                    // logToDisplay("iBeaconUUID = " + _iBeaconUUID +
                    // "\nDistance = " + _proxDistance + "\nproximity = " +
                    // proximity + "\nnotifyUser = " +
                    // pref.getBoolean("notifyUser", false));

                    if (_proxDistance < enterThreshold) {

                        try {
                            if (AccountState.getPrefBeaconId().equals("id_token") || !AccountState.getPrefBeaconId().equals(nearestBeacon.getProximityUUID().toString())) {
                                beaconDetection(nearestBeacon);
                                AccountState.setPrefBeaconId(nearestBeacon.getProximityUUID().toString());
                            }
                        } catch (NullPointerException e) {

                        }


                        try {
                            customerInBoundState = AccountState.getPrefCustomerInbound();

//                            Log.d("@@##", "CustomerIbound Value : " + AccountState.getPrefCustomerInbound());
//                            Log.d("@@##", "CustomerIbound Count : " + callCount);
//                            System.out.println("@@## inbound check " + (!customerInBoundState && _iBeaconUUID.equals("b9407f30-f5f8-466e-aff9-25556b57fe6d") && !AccountState.getUserID().isEmpty() && callCount == 0));
                            if (!customerInBoundState && _iBeaconUUID.equals("b9407f30-f5f8-466e-aff9-25556b57fe6d") && !AccountState.getUserID().isEmpty() && (callCount == 0 || callCount == 2)) {

                                if (callCount == 2) {
//                                    callCount = 0;
                                    AccountState.setPrefCustomerInbound(true);
                                    makeCustomerActiveCall();
                                    try {


                                        wait(10000);
                                    } catch (IllegalMonitorStateException | InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    callCount = 1;
                                    makeCustomerActiveCall();
                                    makeSignalRCall();
                                }
                            }

                            if (_iBeaconUUID.equals("5abe22fe-760c-9c7a-df43-4acc460f377a") && callCount == 1) {
                                AccountState.setPrefCustomerInbound(false);
                                callCount = 2;
                            }


                            JSONObject jsonObject = new JSONObject(
                                    loadJSONFromAsset());
                            JSONArray alertsArry = jsonObject
                                    .getJSONArray("alerts");
                            // logToDisplay("iBeaconUUID = " +
                            // _iBeaconUUID+",alertsArry =  "+alertsArry.length());
                            for (int i = 0; i < alertsArry.length(); i++) {
                                JSONObject alertObject = alertsArry
                                        .getJSONObject(i);
                                String assetId = alertObject
                                        .getString("beaconId");
                                Integer alertId = alertObject.getInt("id");
                                if (_iBeaconUUID.equalsIgnoreCase(assetId)) {
                                    if (isExistInHistory(alertId)) {
                                        Log.d("@@##",
                                                "alert already exist in history");
                                    } else {
                                        Log.d("@@##",
                                                "alert not exist in history, sending new notification");
                                        // send alert to wear
                                        // sendAlert(alertObject);

                                        // Send message to wear
                                        Login_Screen.sendMessage(
                                                alertObject.toString(),
                                                Constants.PATH_NOTIFICATION);

                                        // add alert to history
                                        HashMap<String, Object> map = new HashMap<>();
                                        map.put("alertId", alertId);
                                        map.put("sentAt",
                                                System.currentTimeMillis());
                                        history.add(map);
                                    }

                                } else {
                                    // Log.d("@@##", "ibeacon not in list");
                                }
                            }
                        } catch (NullPointerException | JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } else if (_proxDistance > exitThreshold) {
                    }

                }
            }

        });

        region = new Region("ranged region", null, null, null);
    }

    private void makeSignalRCall() {


        System.out.println("@@##  makeSignalRCall made");

        CustomerGreetingModel customerGreetingModel = new CustomerGreetingModel();


//            System.out.println("@@## AccountState userid" + AccountState.getPrefUserId());
//        addAssociateModel.setAssociateId(Integer.parseInt(AccountState.getPrefUserId()));
        customerGreetingModel.setAssociateId(Integer.parseInt("004024"));
        customerGreetingModel.setCustomerId(Integer.parseInt("1000243"));
        customerGreetingModel.setMessage("Hi.");
        customerGreetingModel.setCaptureTime(dateNtime());
//        addAssociateModel.setFirstName("John");
//        addAssociateModel.setLastName("Doe");


//            dateNtime();

        CustomerSignalRService.sendMessage(gson.toJson(customerGreetingModel), "greeting");


    }

    public void logToDisplay(String toast_msg) {
//		Log.d("@@##", "Service :: " + toast_msg);

    }

    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = getAssets().open("alerts.txt");

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            json = new String(buffer, "UTF-8");
            Alerts alerts = new Gson().fromJson(json, Alerts.class);
//			Log.d("@@##", "alerts size = " + alerts.getAlerts().size());

            for (int i = 0; i < alerts.getAlerts().size(); i++) {
                Alert alert = (Alert) alerts.getAlerts().get(i);
                List<Offer> offers = alert.getOffers();
                for (int j = 0; j < offers.size(); j++) {
                    Offer offer = offers.get(j);
                    // String message = new
                    // String(offer.getDescription().getBytes("UTF-8"),
                    // "UTF-8");
                    // String message =
                    // URLDecoder.decode(offer.getDescription(), "UTF-8");
                    // Log.d("@@##", "offer desc = "
                    // +Html.fromHtml(offer.getDescription()));
//					Log.d("@@##",
//							"offer desc message = " + offer.getDescription());
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public boolean isExistInHistory(Integer aId) {
        boolean result = false;
        for (int j = 0; j < history.size(); j++) {
            HashMap<String, Object> map = history.get(j);
            if (map.get("alertId") == aId) {
                long sentAt = (long) map.get("sentAt");

                Log.d("@@##",
                        "diff = "
                                + Math.abs(System.currentTimeMillis() - sentAt));
                // 3000(millliseconds in a second)*60(seconds in a
                // minute)*30(number of minutes)=5400000
                // if (Math.abs(System.currentTimeMillis() - sentAt) > 5400000)
                // {
                if (Math.abs(System.currentTimeMillis() - sentAt) > 10000) {
                    // history timestamp is not within 30 minutes of current
                    // system time, send notification again
                    Log.d("@@##", "history timestamp is not within 30 minutes");
                    result = false;
                } else {
                    // history timestamp is within 30 minutes of current system
                    // time, don't send notification
                    Log.d("@@##", "history timestamp is within 30 minutes ");
                    result = true;
                }
            }
        }

        return result;
    }

    public void sendAlert(JSONObject alertObject) {
        String alertType = null;
        String offerTitle = null;
        try {
            alertType = alertObject.getString("type");
            offerTitle = alertObject.getString("locationname");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Log.d("@@##", "alertType = " + alertType + ", offerTitle = "
                + offerTitle);

        if (alertType.equalsIgnoreCase("offer")) {
            JSONArray offersArray;
            try {
                offersArray = alertObject.getJSONArray("offers");
                Integer offerCount = offersArray.length();

                // offer notification
                for (int k = 0; k < offerCount; k++) {
                    JSONObject offerObj = offersArray.getJSONObject(k);
                    String offerText = offerObj.getString("offer");

                    Builder notifBuilder = new NotificationCompat.Builder(
                            getBaseContext()).setContentTitle(offerTitle)
                            .setContentText(offerText)
                            .setSmallIcon(R.drawable.retail_logo);

                    if (offerCount > 1) {
                        notifBuilder.setGroup(GROUP_KEY_OFFERS);
                    }

                    // Issue the notification
                    notificationManager.notify((new Random()).nextInt(),
                            notifBuilder.build());
                    Log.d("@@##", "notification sent for offer text = "
                            + offerText);
                }

                // summary notification
                if (offerCount > 1) {
                    Log.d("@@##", "inside summary notif, offerCount =  "
                            + offerCount);

                    // for handheld
                    Notification summaryNotifBuilder = new NotificationCompat.Builder(
                            getBaseContext()).setContentTitle("Discover")
                            .setContentText(offerCount + " Offers near by")
                            .setSmallIcon(R.drawable.retail_mate_app_icon)
                            .setGroup(GROUP_KEY_OFFERS)
                            // .setLocalOnly(true)
                            .setGroupSummary(true).build();
                    notificationManager.notify((new Random()).nextInt(),
                            summaryNotifBuilder);

                    // for wear
                    Builder summaryWearNotifBuilder = new NotificationCompat.Builder(
                            getBaseContext()).setContentTitle("Discover")
                            .setContentText(offerCount + " Offers near by")
                            .setGroup(GROUP_KEY_OFFERS);

                    NotificationCompat.WearableExtender wearableOptions = new NotificationCompat.WearableExtender()
                            .setHintHideIcon(true).setContentIcon(
                                    R.drawable.retail_mate_app_icon);
                    summaryWearNotifBuilder.extend(wearableOptions);

                    notificationManager.notify((new Random()).nextInt(),
                            summaryWearNotifBuilder.build());
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else if (alertType.equalsIgnoreCase("atm")) {

            Log.d("@@##", "notification sent for offer nearest atm");

            Builder atmNotifBuilder = new NotificationCompat.Builder(
                    getBaseContext()).setContentTitle("Discover ATM")
                    .setContentText(70 + " meters away")
                    .setSmallIcon(R.drawable.retail_mate_app_icon);

            // Issue the notification
            notificationManager.notify((new Random()).nextInt(),
                    atmNotifBuilder.build());

        }

    }

    public void makeCustomerActiveCall() {
        Log.d("@@##", "call made to in bound customer");
        Log.d("@@##", "customer ID: " + AccountState.getUserID());

//        if (!(callCount == 2)) {
//            callCount = 1;
//        }
        Log.d("@@##", "CustomerIbound Value : " + AccountState.getPrefCustomerInbound());
        String url = "http://rmlmapi.azurewebsites.net/api/BeaconServices/UpdateActiveCustomer";

        StringRequest req = new StringRequest(Request.Method.PATCH, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("@@## inbound:%n %s", response);
//                    reply=response;


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("Error: ", error.getMessage());
//                callCount = 0;
//                    reply="server not working";
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("UserID", AccountState.getUserID());


                return params;


            }


            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                int mStatusCode = response.statusCode;

                Log.d("@@##", "Response Back " + response.statusCode);
                if (mStatusCode == HttpURLConnection.HTTP_OK) {
                    if (callCount == 2) {
                        AccountState.setPrefCustomerInbound(false);
                        callCount = 0;

//                        new Handler().postDelayed(new Runnable()
//                        {
//                            @Override
//                            public void run()
//                            {
//                                AccountState.setPrefCustomerInbound(false);
//                                callCount = 0;
//                            }
//                        }, 5000);


                    } else {
                        AccountState.setPrefCustomerInbound(true);
                        callCount = 1;

                    }

                } else {
//                    callCount = 0;
                }
                return super.parseNetworkResponse(response);
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);

    }

    public void makeOneRetailWishlistCheckCall() {


        String url = "http://104.43.192.216:8180/api/Cust?CID=%279902%27&SID=%27s1%27&WID=%27w1%27";

        StringRequest req = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("@@## inbound:%n %s", response);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {


            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                int mStatusCode = response.statusCode;

                Log.d("@@##", "Response Back " + response.statusCode);
                if (mStatusCode == HttpURLConnection.HTTP_OK) {

                    AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext())
                            .setTitle("Title")
                            .setMessage("Are you sure?")
                            .create();

                    alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                    alertDialog.show();
                } else {

                }
                return super.parseNetworkResponse(response);
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);

    }


    public void beaconDetection(Beacon beaconID) {

        System.out.println("@@##  NEAREST BEACON" + beaconID.getProximityUUID().toString());

        AddCustomerModel addCustomerModel = new AddCustomerModel();


        System.out.println("@@## AccountState userid" + AccountState.getUserID());
//        addAssociateModel.setAssociateId(Integer.parseInt(AccountState.getPrefUserId()));
        addCustomerModel.setUserId(AccountState.getUserID());
        addCustomerModel.setBeaconId(beaconID.getProximityUUID().toString());
        addCustomerModel.setCaptureTime(dateNtime());
        addCustomerModel.setXPosition(200.0);
        addCustomerModel.setYPosition(200.0);


        dateNtime();

        CustomerSignalRService.sendMessage(gson.toJson(addCustomerModel), "position");


    }


    /*


     */
    public String dateNtime() {
        Date d = new Date();
        CharSequence s = DateFormat.format("yyyy-M-dThh:mm:ss", d.getTime());
        return (String) s;
    }
}
