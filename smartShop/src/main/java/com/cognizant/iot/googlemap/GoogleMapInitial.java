package com.cognizant.iot.googlemap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.retailmate.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class GoogleMapInitial extends AppCompatActivity {
    private static final String[] places = new String[]{"Chennai", "Mumbai",
            "Dubai", "Abu Dhabi", "Al Ain"};

    ProgressDialog prgDialog;

    static double latitude1 = 13.0827;
    static double longitude1 = 80.2707;
    static double latitude2 = 18.9750;
    static double longitude2 = 72.8258;
    // static double latitude3 = 28.6139;
    // static double longitude3 = 77.2090;

    static double latitude3 = 25.209249;
    static double longitude3 = 55.196018;

    static double latitude4 = 24.626379;
    static double longitude4 = 54.344577;

    static double latitude5 = 24.401469;
    static double longitude5 = 55.7508276;

    static double[][] latlogvariable = new double[][]{
            {latitude1, longitude1}, {latitude2, longitude2},
            {latitude3, longitude3}, {latitude4, longitude4}};

    CameraPosition cameraPosition;

    static final LatLng retailpoint1 = new LatLng(latitude1, longitude1);

    static final LatLng retailpoint2 = new LatLng(latitude2, longitude2);

    static final LatLng retailpoint3 = new LatLng(latitude3, longitude3);

    static final LatLng retailpoint4 = new LatLng(latitude4, longitude4);

    private GoogleMap googleMap;

    // Marker TP1,TP2,TP3;
    Marker marker[] = new Marker[5];

    // MarkerOptions marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//		getActionBar().setHomeAsUpIndicator(R.drawable.back_aro);
//		getActionBar().setDisplayHomeAsUpEnabled(true);
//		getActionBar().setHomeButtonEnabled(true);
//		getActionBar().setDisplayShowTitleEnabled(true);
//		getActionBar().setBackgroundDrawable(
//				new ColorDrawable(Color.parseColor("#B5D000")));
//		getActionBar().setIcon(
//				new ColorDrawable(getResources().getColor(
//						android.R.color.transparent)));
//
//		int titleId = getResources().getIdentifier("action_bar_title", "id",
//				"android");
//
//		TextView abTitle = (TextView) findViewById(titleId);
//		abTitle.setTextColor(Color.parseColor("#ffffff"));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.google_initial_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarGoogleMapintial);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        Toast.makeText(getApplicationContext(),
                "Showing all retailmates on google maps", Toast.LENGTH_SHORT)
                .show();
        try {
            // Loading map
            initilizeMap();

            // Changing map type
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            // Showing / hiding your current location
            // googleMap.setMyLocationEnabled(true);

            // Enable / Disable zooming controls
            googleMap.getUiSettings().setZoomControlsEnabled(false);

            // Enable / Disable my location button
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);

            // Enable / Disable Compass icon
            googleMap.getUiSettings().setCompassEnabled(true);

            // Enable / Disable Rotate gesture
            googleMap.getUiSettings().setRotateGesturesEnabled(true);

            // Enable / Disable zooming functionality
            googleMap.getUiSettings().setZoomGesturesEnabled(true);

            marker[0] = googleMap.addMarker(new MarkerOptions().position(
                    retailpoint1).title("Retail Mate at Chennai"));

            marker[1] = googleMap.addMarker(new MarkerOptions().position(
                    retailpoint2).title("Retail Mate at Mumbai"));

            marker[2] = googleMap.addMarker(new MarkerOptions().position(
                    retailpoint3).title("Retail Mate at Dubai"));

            marker[3] = googleMap.addMarker(new MarkerOptions().position(
                    retailpoint3).title("Retail Mate at Abu Dhabi"));

            marker[4] = googleMap.addMarker(new MarkerOptions().position(
                    retailpoint3).title("Retail Mate at Al Ain"));

            // Log.e("Random", "> " + randomLocation[0] + ", "+
            // randomLocation[1]);

            marker[0].setIcon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
            marker[1].setIcon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            marker[2].setIcon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            marker[3].setIcon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_CYAN));

            LatLng myLocation = callToGetMyLocation();
            marker[4] = googleMap.addMarker(new MarkerOptions().position(
                    myLocation).title("My Current Location"));
            marker[4].setIcon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_RED));

            // googleMap.addMarker(marker[0]);
            // googleMap.addMarker(marker[1]);
            // googleMap.addMarker(marker[2]);
            // googleMap.addMarker(marker[3]);

            // for (int i = 0; i < 3; i++) {
            // for (int j = 0; j < 1; j++) {
            // cameraPosition = new CameraPosition.Builder()
            // .target(new LatLng(latlogvariable[i][j],
            // latlogvariable[i][j + 1])).zoom(15).build();
            //
            // }
            // }
            // googleMap.animateCamera(CameraUpdateFactory
            // .newCameraPosition(cameraPosition));
            LatLngBounds.Builder b = new LatLngBounds.Builder();
            for (Marker m : marker) {
                b.include(m.getPosition());
            }
            LatLngBounds bounds = b.build();
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,
                    17));

        } catch (Exception e) {
            e.printStackTrace();
        }

        final AutoCompleteTextView auto = (AutoCompleteTextView) findViewById(R.id.Map_initial_auto);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, places);
        auto.setAdapter(adapter);
        Button buttonObject = (Button) findViewById(R.id.button_google_initial);
        buttonObject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchingFunctionalityCall();

            }
        });

        // Map code
        /*
		 * try { if (googleMap == null) { googleMap = ((MapFragment)
		 * getFragmentManager
		 * ().findFragmentById(R.id.map_fragment_initial)).getMap(); }
		 * googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
		 * 
		 * marker[0] = googleMap.addMarker(new
		 * MarkerOptions().position(retailpoint1).title("Retail Mate[1]"));
		 * marker
		 * [0].setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory
		 * .HUE_GREEN));
		 * 
		 * marker[1] = googleMap.addMarker(new
		 * MarkerOptions().position(retailpoint2).title("Retail Mate[2]"));
		 * marker
		 * [1].setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory
		 * .HUE_BLUE));
		 * 
		 * marker[2] = googleMap.addMarker(new
		 * MarkerOptions().position(retailpoint3).title("Retail Mate[3]"));
		 * marker
		 * [2].setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory
		 * .HUE_ORANGE));
		 * 
		 * LatLng myLocation=callToGetMyLocation();
		 * marker[3]=googleMap.addMarker(new
		 * MarkerOptions().position(myLocation).title("MyLocation"));
		 * marker[3].setIcon
		 * (BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory
		 * .HUE_RED));
		 * 
		 * //googleMap.setMyLocationEnabled(true);
		 * googleMap.getUiSettings().setZoomControlsEnabled(true);
		 * googleMap.getUiSettings().setCompassEnabled(true);
		 * googleMap.getUiSettings().setMyLocationButtonEnabled(true);
		 * googleMap.getUiSettings().setRotateGesturesEnabled(true);
		 * 
		 * //Calculate the markers to get their position LatLngBounds.Builder b
		 * = new LatLngBounds.Builder(); for (Marker m : marker) {
		 * b.include(m.getPosition()); } LatLngBounds bounds = b.build();
		 * //Change the padding as per needed CameraUpdate cu =
		 * CameraUpdateFactory.newLatLngBounds(bounds, 25,25,5);
		 * googleMap.animateCamera(cu);
		 * 
		 * } catch (Exception e) { e.printStackTrace(); }
		 * 
		 * final AutoCompleteTextView auto=(AutoCompleteTextView)
		 * findViewById(R.id.Map_initial_auto); ArrayAdapter<String> adapter =
		 * new ArrayAdapter<String>(this,
		 * android.R.layout.simple_dropdown_item_1line, places);
		 * auto.setAdapter(adapter); Button buttonObject=(Button)
		 * findViewById(R.id.button_google_initial);
		 * buttonObject.setOnClickListener(new View.OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { searchingFunctionalityCall();
		 * 
		 * } });
		 */
    }

    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map_fragment_initial)).getMap();

            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    public LatLng callToGetMyLocation() {
        LatLng myPosition;
        double latitude, longitude;
        googleMap.setMyLocationEnabled(true);

        // Getting LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
        Location location = locationManager.getLastKnownLocation(provider);

        // if(location!=null)

        // Getting latitude of the current location
        latitude = location.getLatitude();

        // Getting longitude of the current location
        longitude = location.getLongitude();

        // Creating a LatLng object for the current location
        // LatLng latLng = new LatLng(latitude, longitude);\

        myPosition = new LatLng(latitude, longitude);

        return myPosition;
    }

    public void searchingFunctionalityCall() {
        AutoCompleteTextView object = (AutoCompleteTextView) findViewById(R.id.Map_initial_auto);
        String searchParameter = object.getText().toString();
        if (searchParameter.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Search field empty",
                    Toast.LENGTH_SHORT).show();

        } else {
            Locations currentLoc = Locations.valueOf(searchParameter);
            Intent finalGoogle;
            finalGoogle = new Intent(getApplicationContext(),
                    GoogleMapFinal.class);
            switch (currentLoc) {
                case Chennai:

                    finalGoogle.putExtra("PlaceName", searchParameter);
                    finalGoogle.putExtra("latitude", latitude1);
                    finalGoogle.putExtra("longitude", longitude1);
                    break;

                case Mumbai:

                    finalGoogle.putExtra("PlaceName", searchParameter);
                    finalGoogle.putExtra("latitude", latitude2);
                    finalGoogle.putExtra("longitude", longitude2);

                    break;

                case Dubai:

                    finalGoogle.putExtra("PlaceName", searchParameter);
                    finalGoogle.putExtra("latitude", latitude3);
                    finalGoogle.putExtra("longitude", longitude3);

                    break;

                case AbuDhabi:

                    finalGoogle.putExtra("PlaceName", searchParameter);
                    finalGoogle.putExtra("latitude", latitude4);
                    finalGoogle.putExtra("longitude", longitude4);

                    break;
                case AlAin:

                    finalGoogle.putExtra("PlaceName", searchParameter);
                    finalGoogle.putExtra("latitude", latitude5);
                    finalGoogle.putExtra("longitude", longitude5);

                    break;
                default:

                    Toast.makeText(getApplicationContext(),
                            "Retail Mate not available in this location",
                            Toast.LENGTH_SHORT).show();

                    break;
            }
            startActivity(finalGoogle);
            // finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        AutoCompleteTextView object1 = (AutoCompleteTextView) findViewById(R.id.Map_initial_auto);
        object1.setText("");
        initilizeMap();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // setMenuBackground();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}// end of class
