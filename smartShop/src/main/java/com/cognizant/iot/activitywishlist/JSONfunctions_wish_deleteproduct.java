package com.cognizant.iot.activitywishlist;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UTFDataFormatException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.utils.MySSLSocketFactory;
import com.google.gson.*;

import android.util.Log;

public class JSONfunctions_wish_deleteproduct {

	public static JSONObject getJSONfromURLAXADDwishlist(String url,
			String pid, String itemid) {
		InputStream is = null;
		String result = "";
		JSONObject jArray = null;
		ArrayList<NameValuePair> postParameters;

		HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();

		System.out.println("@@## URL in get call " + url);
		HttpPost httpPost = new HttpPost(url);


//		httpPost.setHeader("Authorization", CatalogActivity.google_token);
//		httpPost.addHeader(
//				"OUN",
//				"052");
		//httpPost.setHeader("Authorization", CatalogActivity.access_token_AX);
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");

		Long proID = Long.parseLong(pid);

		JSONObject jo = new JSONObject();
		try {
			//jo.put("Customer", "2001");
			jo.put("ProductId", proID);
			jo.put("Quantity", 1);
			StringEntity se = new StringEntity(jo.toString());
			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			HttpEntity entity;


//				entity = new StringEntity("{\"Customer\":" + "\"2001\""
//						+ "\"Product\"" + "\"" + proID + "\""
//						+ ",\"product_name\":" + "\"" + itemid + "\"" + "}");

				// System.out.println("@@## ENTITY \n" + entity);

				httpPost.setEntity(se);


		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// System.out.println("@@## access_token_AX "
		// + CatalogActivity.access_token_AX);

		try {
			HttpResponse httpresponse = httpclient.execute(httpPost);
			System.out.println("@@@@@@@@@@@@@@@@@@@@"
					+ httpresponse.getStatusLine().toString());

			HttpEntity httpentity = httpresponse.getEntity();
			is = httpentity.getContent();

			System.out.println("@@### " + is.toString());

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		try {

			jArray = new JSONObject(result);
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}

		return jArray;
	}

	public static JSONObject getJSONfromURLAX(String url) {
		InputStream is = null;
		String result = "";
		JSONObject jArray = null;
		ArrayList<NameValuePair> postParameters;

		// Download JSON data from URL
		try {
			HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
			HttpDelete httpdelete = new HttpDelete(url);
			httpdelete.addHeader("Authorization",
					CatalogActivity.access_token_AX);
			// Set HTTP parameters
			httpdelete.setHeader("Accept", "application/json");
			httpdelete.setHeader("Content-type", "application/json");


			HttpResponse response = httpclient.execute(httpdelete);
			HttpEntity httpentity = response.getEntity();
			is = httpentity.getContent();

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// Convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		try {

			jArray = new JSONObject(result);
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}

		return jArray;
	}

	public static JSONObject getJSONfromURL(String url, String prodid) {
		InputStream is = null;
		String result = "";
		JSONObject jArray = null;
		ArrayList<NameValuePair> postParameters;

		// Download JSON data from URL
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("macaddr", CatalogActivity.imei));
			params.add(new BasicNameValuePair("products", prodid));
			// params.add(new BasicNameValuePair("pass", "xyz"));
			UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,
					HTTP.UTF_8);
			httppost.setEntity(ent);

			/*
			 * httppost.setHeader("Content-Type", "application/json");
			 * HttpEntity entity = new
			 * StringEntity("{\"macaddr\":\" 000000000000788 \"}");
			 * httppost.setEntity(entity);
			 */

			HttpResponse response = httpclient.execute(httppost);
			HttpEntity httpentity = response.getEntity();
			is = httpentity.getContent();

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// Convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		try {

			jArray = new JSONObject(result);
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}

		return jArray;
	}
}
