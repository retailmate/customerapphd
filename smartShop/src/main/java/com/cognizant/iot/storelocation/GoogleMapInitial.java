package com.cognizant.iot.storelocation;


import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.cognizant.retailmate.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class GoogleMapInitial extends AppCompatActivity implements OnMapReadyCallback {
    private static final String[] places = new String[]{"Chennai", "Mumbai",
            "Dubai", "Abu Dhabi", "Al Ain"};

    private static final String CustomTag = "CustomTag";

    private MapFragment mMapFrag;
    Map<String, Double> latMap = new HashMap<String, Double>();
    Map<String, Double> lngMap = new HashMap<String, Double>();
    private static final float DEFAULT_ZOOM = 5;
    private LatLng userLocation;

    private GoogleMap googleMap;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_map_initial);

        toolbar = (Toolbar) findViewById(R.id.toolbarGoogleMapintial);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        Toast.makeText(getApplicationContext(),
                "Showing all retailmates on google maps", Toast.LENGTH_SHORT)
                .show();

        String jsonString = loadJSONFromAsset();


        try {
            //Reading Location in form of JSON String returned by loadJSONFromAsset()
            JSONObject obj = new JSONObject(jsonString);

            // Creating JSONArray from JSONObject
            JSONArray jsonArray = obj.getJSONArray("locations");

            for (int i = 0; i < jsonArray.length(); i++) {

                // Creating JSONObject from JSONArray
                JSONObject jsonObj = jsonArray.getJSONObject(i);

                // Getting data from individual JSONObject

                String place = jsonObj.getString("place");
                Double lat = jsonObj.getDouble("lat");
                Double lng = jsonObj.getDouble("lng");

                latMap.put(place, lat);
                lngMap.put(place, lng);
                Log.d(CustomTag, "place = " + place + " lat = " + latMap.get(place) + " lng = " + lngMap.get(place));

            }

            // Loading map
            initilizeMap();


        } catch (Exception e) {
            e.printStackTrace();
        }

        final AutoCompleteTextView auto = (AutoCompleteTextView) findViewById(R.id.Map_initial_auto);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, places);
        auto.setAdapter(adapter);
        final Button buttonObject = (Button) findViewById(R.id.button_google_initial);
        buttonObject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //hide keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(buttonObject.getApplicationWindowToken(), 0);
                //search RetailMate Stores.
                searchingFunctionalityCall();

            }
        });


    }

    private void initilizeMap() {
        if (googleMap == null) {
            mMapFrag = (MapFragment) getFragmentManager().findFragmentById(
                    R.id.map_fragment_initial);
            mMapFrag.getMapAsync(this);


        }
    }

    @Override
    public void onMapReady(GoogleMap Map) {
        googleMap = Map;

        // Changing map type
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        // getting user location
        //userLocation=callToGetMyLocation();

        // Enable / Disable zooming controls
        googleMap.getUiSettings().setZoomControlsEnabled(false);

        // Enable / Disable my location button
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        // Enable / Disable Compass icon
        googleMap.getUiSettings().setCompassEnabled(true);

        // Enable / Disable Rotate gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);

        // Enable / Disable zooming functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);

        //Adding Markers
        for (Map.Entry m : latMap.entrySet()) {
            MarkerOptions options = new MarkerOptions()
                    .title("RetailMate Store at " + m.getKey().toString())
                    .position(new LatLng(latMap.get(m.getKey()), lngMap.get(m.getKey())));
            googleMap.addMarker(options);
        }

        if (googleMap == null) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();
        }
    }


    public LatLng callToGetMyLocation() {
        LatLng myPosition = null;
        double latitude, longitude;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
            // Getting LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();

            // Getting the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);

            // Getting Current Location
            Location location = locationManager.getLastKnownLocation(provider);

            // if(location!=null)

            // Getting latitude of the current location
            latitude = location.getLatitude();

            // Getting longitude of the current location
            longitude = location.getLongitude();
            myPosition = new LatLng(latitude, longitude);
        }
        //googleMap.setMyLocationEnabled(true);
            /*// Getting LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

			// Creating a criteria object to retrieve provider
			Criteria criteria = new Criteria();

			// Getting the name of the best provider
			String provider = locationManager.getBestProvider(criteria, true);

			// Getting Current Location
			Location location = locationManager.getLastKnownLocation(provider);

			// if(location!=null)

			// Getting latitude of the current location
			latitude = location.getLatitude();

			// Getting longitude of the current location
			longitude = location.getLongitude();*/

        // Creating a LatLng object for the current location
        // LatLng latLng = new LatLng(latitude, longitude);\

        //myPosition = new LatLng(latitude, longitude);


        return myPosition;
    }

    public void searchingFunctionalityCall() {
        AutoCompleteTextView object = (AutoCompleteTextView) findViewById(R.id.Map_initial_auto);
        String searchParameter = object.getText().toString();
        if (searchParameter.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Search field empty",
                    Toast.LENGTH_SHORT).show();

        } else {

            if (latMap.containsKey(searchParameter))
                gotoLocation(searchParameter, DEFAULT_ZOOM);
            else
                Toast.makeText(getApplicationContext(), "No RetailMate Stores Found.",
                        Toast.LENGTH_SHORT).show();
        }
    }

    //Updating Camera Location
    public void gotoLocation(String place, float zoom) {
        LatLng ll = new LatLng(latMap.get(place), lngMap.get(place));
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, zoom);
        googleMap.animateCamera(update);
    }

    @Override
    public void onResume() {
        super.onResume();
        AutoCompleteTextView object1 = (AutoCompleteTextView) findViewById(R.id.Map_initial_auto);
        object1.setText("");
        initilizeMap();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // setMenuBackground();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = getAssets().open("location.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }


}// end of class
