package com.cognizant.iot.extrascreens;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.cognizant.retailmate.R;


public class CustomerWelcomeBanner extends AppCompatActivity {
    Button dismiss_button, ok_button, close_button;

    LinearLayout welcome_layout, bought_product_layout, thanks_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homedepot_extra_activity_associate_details_pop_up);

        dismiss_button = (Button) findViewById(R.id.dismiss_button);
        ok_button = (Button) findViewById(R.id.ok_button);
        close_button = (Button) findViewById(R.id.close_button);

        welcome_layout = (LinearLayout) findViewById(R.id.welcome_layout);
        bought_product_layout = (LinearLayout) findViewById(R.id.bought_product_layout);
        thanks_layout = (LinearLayout) findViewById(R.id.thanks_layout);

        dismiss_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                welcome_layout.setVisibility(View.GONE);
                bought_product_layout.setVisibility(View.VISIBLE);
                thanks_layout.setVisibility(View.GONE);
            }
        });
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                welcome_layout.setVisibility(View.GONE);
                bought_product_layout.setVisibility(View.GONE);
                thanks_layout.setVisibility(View.VISIBLE);
            }
        });
        close_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void finishActivity(View view) {
        finish();
    }

}
