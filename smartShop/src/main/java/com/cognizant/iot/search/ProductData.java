package com.cognizant.iot.search;

public class ProductData 
{
	String id,name,desc,image_url,price;
    
    public ProductData(String id, String name, String price, String desc,String imageurl) 
    {
        this.setId(id);
        this.setName(name);
        this.setDesc(desc);
        this.setImageUrl(imageurl);
        this.setPrice(price);
    }
 
    public void setImageUrl(String imageurl) 
    {
        this.image_url =imageurl;
    }
 
    public String getImageUrl() 
    {
        return image_url;
    }
 
    public void setPrice(String price) 
    {
        this.price = price;
    }
    public String getPrice() 
    {
        return price;
    }
    public String getId() 
    {
        return id;
    }
 
    public void setId(String id) 
    {
        this.id = id;
    }
    public String getName() 
    {
        return name;
    }
 
    public void setName(String name) 
    {
        this.name = name;
    }
    
    public void setDesc(String desc) 
    {
        this.desc = desc;
    }
    
    public String getDesc() 
    {
        return desc;
    } 
}
