package com.cognizant.iot.homepage.models.cart;

/**
 * Created by 452781 on 2/23/2017.
 */

import com.cognizant.iot.homepage.models.productsearch.ProductSearchValueModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CartLineModel implements Serializable {

    @SerializedName("LineId")
    @Expose
    private String lineId;
    @SerializedName("TaxOverrideCode")
    @Expose
    private Object taxOverrideCode;
    @SerializedName("ItemId")
    @Expose
    private String itemId;
    @SerializedName("Barcode")
    @Expose
    private String barcode;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("InventoryDimensionId")
    @Expose
    private String inventoryDimensionId;
    @SerializedName("Comment")
    @Expose
    private String comment;
    @SerializedName("ProductId")
    @Expose
    private String productId;
    @SerializedName("WarehouseId")
    @Expose
    private String warehouseId;
    @SerializedName("Quantity")
    @Expose
    private Integer quantity;
    @SerializedName("Price")
    @Expose
    private Double price;
    @SerializedName("ExtendedPrice")
    @Expose
    private Double extendedPrice;
    @SerializedName("TaxAmount")
    @Expose
    private Double taxAmount;
    @SerializedName("ItemTaxGroupId")
    @Expose
    private String itemTaxGroupId;
    @SerializedName("TotalAmount")
    @Expose
    private Double totalAmount;
    @SerializedName("NetAmountWithoutTax")
    @Expose
    private Double netAmountWithoutTax;
    @SerializedName("DiscountAmount")
    @Expose
    private Double discountAmount;
    @SerializedName("LineDiscount")
    @Expose
    private Integer lineDiscount;
    @SerializedName("LinePercentageDiscount")
    @Expose
    private Integer linePercentageDiscount;
    @SerializedName("LineManualDiscountPercentage")
    @Expose
    private Integer lineManualDiscountPercentage;
    @SerializedName("LineManualDiscountAmount")
    @Expose
    private Integer lineManualDiscountAmount;
    @SerializedName("UnitOfMeasureSymbol")
    @Expose
    private String unitOfMeasureSymbol;
    @SerializedName("ShippingAddress")
    @Expose
    private Object shippingAddress;
    @SerializedName("DeliveryMode")
    @Expose
    private String deliveryMode;
    @SerializedName("DeliveryModeChargeAmount")
    @Expose
    private Object deliveryModeChargeAmount;
    @SerializedName("RequestedDeliveryDate")
    @Expose
    private Object requestedDeliveryDate;
    @SerializedName("ReturnTransactionId")
    @Expose
    private String returnTransactionId;
    @SerializedName("ReturnLineNumber")
    @Expose
    private Integer returnLineNumber;
    @SerializedName("ReturnInventTransId")
    @Expose
    private String returnInventTransId;
    @SerializedName("IsVoided")
    @Expose
    private Boolean isVoided;
    @SerializedName("IsGiftCardLine")
    @Expose
    private Boolean isGiftCardLine;
    @SerializedName("IsPriceKeyedIn")
    @Expose
    private Boolean isPriceKeyedIn;
    @SerializedName("SalesStatusValue")
    @Expose
    private Integer salesStatusValue;
    @SerializedName("QuantityOrdered")
    @Expose
    private Object quantityOrdered;
    @SerializedName("QuantityInvoiced")
    @Expose
    private Object quantityInvoiced;
    @SerializedName("FulfillmentStoreId")
    @Expose
    private String fulfillmentStoreId;
    @SerializedName("SerialNumber")
    @Expose
    private String serialNumber;
    @SerializedName("ElectronicDeliveryEmail")
    @Expose
    private String electronicDeliveryEmail;
    @SerializedName("ElectronicDeliveryEmailContent")
    @Expose
    private String electronicDeliveryEmailContent;
    @SerializedName("IsPriceOverridden")
    @Expose
    private Boolean isPriceOverridden;
    @SerializedName("OriginalPrice")
    @Expose
    private Object originalPrice;
    @SerializedName("IsInvoiceLine")
    @Expose
    private Boolean isInvoiceLine;
    @SerializedName("InvoiceId")
    @Expose
    private String invoiceId;
    @SerializedName("InvoiceAmount")
    @Expose
    private Integer invoiceAmount;
    @SerializedName("PromotionLines")
    @Expose
    private List<Object> promotionLines = null;
    @SerializedName("CartDiscountLineModels")
    @Expose
    private List<CartDiscountLineModel> discountLines = null;
    @SerializedName("ReasonCodeLines")
    @Expose
    private List<Object> reasonCodeLines = null;
    @SerializedName("ChargeLines")
    @Expose
    private List<Object> chargeLines = null;
    @SerializedName("TaxRatePercent")
    @Expose
    private Double taxRatePercent;
    @SerializedName("IsCustomerAccountDeposit")
    @Expose
    private Boolean isCustomerAccountDeposit;
    @SerializedName("LinkedParentLineId")
    @Expose
    private Object linkedParentLineId;
    @SerializedName("ExtensionProperties")
    @Expose
    private List<Object> extensionProperties = null;


    ProductSearchValueModel productSearchValueModel;

    public ProductSearchValueModel getProductSearchValueModel() {
        return productSearchValueModel;
    }

    public void setProductSearchValueModel(ProductSearchValueModel productSearchValueModel) {
        this.productSearchValueModel = productSearchValueModel;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public Object getTaxOverrideCode() {
        return taxOverrideCode;
    }

    public void setTaxOverrideCode(Object taxOverrideCode) {
        this.taxOverrideCode = taxOverrideCode;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInventoryDimensionId() {
        return inventoryDimensionId;
    }

    public void setInventoryDimensionId(String inventoryDimensionId) {
        this.inventoryDimensionId = inventoryDimensionId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getExtendedPrice() {
        return extendedPrice;
    }

    public void setExtendedPrice(Double extendedPrice) {
        this.extendedPrice = extendedPrice;
    }

    public Double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(Double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getItemTaxGroupId() {
        return itemTaxGroupId;
    }

    public void setItemTaxGroupId(String itemTaxGroupId) {
        this.itemTaxGroupId = itemTaxGroupId;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getNetAmountWithoutTax() {
        return netAmountWithoutTax;
    }

    public void setNetAmountWithoutTax(Double netAmountWithoutTax) {
        this.netAmountWithoutTax = netAmountWithoutTax;
    }

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Integer getLineDiscount() {
        return lineDiscount;
    }

    public void setLineDiscount(Integer lineDiscount) {
        this.lineDiscount = lineDiscount;
    }

    public Integer getLinePercentageDiscount() {
        return linePercentageDiscount;
    }

    public void setLinePercentageDiscount(Integer linePercentageDiscount) {
        this.linePercentageDiscount = linePercentageDiscount;
    }

    public Integer getLineManualDiscountPercentage() {
        return lineManualDiscountPercentage;
    }

    public void setLineManualDiscountPercentage(Integer lineManualDiscountPercentage) {
        this.lineManualDiscountPercentage = lineManualDiscountPercentage;
    }

    public Integer getLineManualDiscountAmount() {
        return lineManualDiscountAmount;
    }

    public void setLineManualDiscountAmount(Integer lineManualDiscountAmount) {
        this.lineManualDiscountAmount = lineManualDiscountAmount;
    }

    public String getUnitOfMeasureSymbol() {
        return unitOfMeasureSymbol;
    }

    public void setUnitOfMeasureSymbol(String unitOfMeasureSymbol) {
        this.unitOfMeasureSymbol = unitOfMeasureSymbol;
    }

    public Object getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Object shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public Object getDeliveryModeChargeAmount() {
        return deliveryModeChargeAmount;
    }

    public void setDeliveryModeChargeAmount(Object deliveryModeChargeAmount) {
        this.deliveryModeChargeAmount = deliveryModeChargeAmount;
    }

    public Object getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    public void setRequestedDeliveryDate(Object requestedDeliveryDate) {
        this.requestedDeliveryDate = requestedDeliveryDate;
    }

    public String getReturnTransactionId() {
        return returnTransactionId;
    }

    public void setReturnTransactionId(String returnTransactionId) {
        this.returnTransactionId = returnTransactionId;
    }

    public Integer getReturnLineNumber() {
        return returnLineNumber;
    }

    public void setReturnLineNumber(Integer returnLineNumber) {
        this.returnLineNumber = returnLineNumber;
    }

    public String getReturnInventTransId() {
        return returnInventTransId;
    }

    public void setReturnInventTransId(String returnInventTransId) {
        this.returnInventTransId = returnInventTransId;
    }

    public Boolean getIsVoided() {
        return isVoided;
    }

    public void setIsVoided(Boolean isVoided) {
        this.isVoided = isVoided;
    }

    public Boolean getIsGiftCardLine() {
        return isGiftCardLine;
    }

    public void setIsGiftCardLine(Boolean isGiftCardLine) {
        this.isGiftCardLine = isGiftCardLine;
    }

    public Boolean getIsPriceKeyedIn() {
        return isPriceKeyedIn;
    }

    public void setIsPriceKeyedIn(Boolean isPriceKeyedIn) {
        this.isPriceKeyedIn = isPriceKeyedIn;
    }

    public Integer getSalesStatusValue() {
        return salesStatusValue;
    }

    public void setSalesStatusValue(Integer salesStatusValue) {
        this.salesStatusValue = salesStatusValue;
    }

    public Object getQuantityOrdered() {
        return quantityOrdered;
    }

    public void setQuantityOrdered(Object quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public Object getQuantityInvoiced() {
        return quantityInvoiced;
    }

    public void setQuantityInvoiced(Object quantityInvoiced) {
        this.quantityInvoiced = quantityInvoiced;
    }

    public String getFulfillmentStoreId() {
        return fulfillmentStoreId;
    }

    public void setFulfillmentStoreId(String fulfillmentStoreId) {
        this.fulfillmentStoreId = fulfillmentStoreId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getElectronicDeliveryEmail() {
        return electronicDeliveryEmail;
    }

    public void setElectronicDeliveryEmail(String electronicDeliveryEmail) {
        this.electronicDeliveryEmail = electronicDeliveryEmail;
    }

    public String getElectronicDeliveryEmailContent() {
        return electronicDeliveryEmailContent;
    }

    public void setElectronicDeliveryEmailContent(String electronicDeliveryEmailContent) {
        this.electronicDeliveryEmailContent = electronicDeliveryEmailContent;
    }

    public Boolean getIsPriceOverridden() {
        return isPriceOverridden;
    }

    public void setIsPriceOverridden(Boolean isPriceOverridden) {
        this.isPriceOverridden = isPriceOverridden;
    }

    public Object getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Object originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Boolean getIsInvoiceLine() {
        return isInvoiceLine;
    }

    public void setIsInvoiceLine(Boolean isInvoiceLine) {
        this.isInvoiceLine = isInvoiceLine;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Integer getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(Integer invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public List<Object> getPromotionLines() {
        return promotionLines;
    }

    public void setPromotionLines(List<Object> promotionLines) {
        this.promotionLines = promotionLines;
    }

    public List<CartDiscountLineModel> getCartDiscountLineModels() {
        return discountLines;
    }

    public void setCartDiscountLineModels(List<CartDiscountLineModel> discountLines) {
        this.discountLines = discountLines;
    }

    public List<Object> getReasonCodeLines() {
        return reasonCodeLines;
    }

    public void setReasonCodeLines(List<Object> reasonCodeLines) {
        this.reasonCodeLines = reasonCodeLines;
    }

    public List<Object> getChargeLines() {
        return chargeLines;
    }

    public void setChargeLines(List<Object> chargeLines) {
        this.chargeLines = chargeLines;
    }

    public Double getTaxRatePercent() {
        return taxRatePercent;
    }

    public void setTaxRatePercent(Double taxRatePercent) {
        this.taxRatePercent = taxRatePercent;
    }

    public Boolean getIsCustomerAccountDeposit() {
        return isCustomerAccountDeposit;
    }

    public void setIsCustomerAccountDeposit(Boolean isCustomerAccountDeposit) {
        this.isCustomerAccountDeposit = isCustomerAccountDeposit;
    }

    public Object getLinkedParentLineId() {
        return linkedParentLineId;
    }

    public void setLinkedParentLineId(Object linkedParentLineId) {
        this.linkedParentLineId = linkedParentLineId;
    }

    public List<Object> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<Object> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

}

