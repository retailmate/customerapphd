package com.cognizant.iot.homepage.fragments;

/**
 * Created by 599584 on 2/17/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.homepage.ProductSearchAPI;
import com.cognizant.iot.homepage.models.productlist.ProductListValueModel;
import com.cognizant.iot.productdetails.activity.ProductDetailsActivity;
import com.cognizant.retailmate.R;

import java.util.ArrayList;
import java.util.List;


public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView productname;
    public ImageView Photo, wishlist_btn1, tag_cart_icon;
    public TextView prodprice, category;
    Context context;
    String productID, type;
    List<ProductListValueModel> productlist;


    public RecyclerViewHolders(View itemView, Context context, List<ProductListValueModel> productList) {

        super(itemView);
        this.context = context;
        this.productlist = productList;

        itemView.setOnClickListener(this);


        this.productname = (TextView) itemView.findViewById(R.id.prodname);
        this.Photo = (ImageView) itemView.findViewById(R.id.flag);
        this.prodprice = (TextView) itemView.findViewById(R.id.prodprice);
        this.category = (TextView) itemView.findViewById(R.id.category);
        this.tag_cart_icon = (ImageView) itemView.findViewById(R.id.tag_cart_icon);
        this.wishlist_btn1 = (ImageView) itemView.findViewById(R.id.wishlist_btn1);


        tag_cart_icon.setOnClickListener(this);
        wishlist_btn1.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        ProductListValueModel productListValueModel = new ProductListValueModel();
        int position = getAdapterPosition();
        productListValueModel = productlist.get(position);

        if (view.getId() == R.id.tag_cart_icon) {

            if (R.drawable.cart_red_icon == tag_cart_icon.getId()) {
                tag_cart_icon.setImageResource(R.drawable.cart_grey);
                tag_cart_icon.setColorFilter(context.getResources().getColor(R.color.bgdark));

            } else {
                tag_cart_icon.setImageResource(R.drawable.cart_red_icon);
                tag_cart_icon.setColorFilter(null);
            }

            List<String> list = new ArrayList<String>();
            list.add(productListValueModel.getRecordId());
            ProductSearchAPI productSearchAPI = new ProductSearchAPI(context, list, "cart");

            productSearchAPI.makeaCall();

        } else if (view.getId() == R.id.wishlist_btn1) {

            if (R.drawable.hearticonnn == wishlist_btn1.getId()) {
                wishlist_btn1.setImageResource(R.drawable.heart_grey);
                wishlist_btn1.setColorFilter(context.getResources().getColor(R.color.bgdark));
            } else {
                wishlist_btn1.setImageResource(R.drawable.hearticonnn);
                wishlist_btn1.setColorFilter(null);
            }

            List<String> list = new ArrayList<String>();
            list.add(productListValueModel.getRecordId());
            ProductSearchAPI productSearchAPI = new ProductSearchAPI(context, list, "wishlist");

            productSearchAPI.makeaCall();
        } else {

            Intent intent = new Intent(context,
                    ProductDetailsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // Pass all data prodid
            intent.putExtra("prodid", productListValueModel.getRecordId());
            // Pass all data prodname
            intent.putExtra("prodname",
                    productListValueModel.getName());
            // Pass all data prodprice
            intent.putExtra("prodprice",
                    productListValueModel.getPrice().toString());
            // Pass all data flag
            intent.putExtra("flag", productListValueModel.getPrimaryImageUrl());
            intent.putExtra("desc", productListValueModel.getName());


//            intent.putExtra("offer", resultp.get(MainActivityprod.OFFER));
            intent.putExtra("category",
                    productListValueModel.getName());

            // Start SingleItemView Class
            context.startActivity(intent);
        }


    }


}