package com.cognizant.iot.homepage.models.productlist;

/**
 * Created by 452781 on 2/21/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductCategoryNameTranslationModel {

    @SerializedName("Language")
    @Expose
    private String language;
    @SerializedName("Text")
    @Expose
    private String text;
    @SerializedName("ExtensionProperties")
    @Expose
    private List<Object> extensionProperties = null;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Object> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<Object> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

}