package com.cognizant.iot.homepage.models.productsearch;

/**
 * Created by 452781 on 2/21/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductSearchRetailContextModel {

    @SerializedName("ChannelId")
    @Expose
    private Integer channelId;
    @SerializedName("CatalogId")
    @Expose
    private Integer catalogId;

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public Integer getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Integer catalogId) {
        this.catalogId = catalogId;
    }

}
