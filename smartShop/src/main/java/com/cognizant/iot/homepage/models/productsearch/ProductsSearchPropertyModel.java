package com.cognizant.iot.homepage.models.productsearch;

/**
 * Created by 452781 on 2/21/2017.
 */


import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductsSearchPropertyModel {

    @SerializedName("TranslationLanguage")
    @Expose
    private String translationLanguage;
    @SerializedName("TranslatedProperties")
    @Expose
    private List<ProductSearchProductPropertyModel> translatedProperties = null;

    public String getTranslationLanguage() {
        return translationLanguage;
    }

    public void setTranslationLanguage(String translationLanguage) {
        this.translationLanguage = translationLanguage;
    }

    public List<ProductSearchProductPropertyModel> getTranslatedProperties() {
        return translatedProperties;
    }

    public void setTranslatedProperties(List<ProductSearchProductPropertyModel> translatedProperties) {
        this.translatedProperties = translatedProperties;
    }

}