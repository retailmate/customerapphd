package com.cognizant.iot.homepage.models.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 452781 on 2/23/2017.
 */

public class CartBreifDataModel {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("CartData")
    @Expose
    private CartDatamodel cartData;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CartDatamodel getCartDatamodel() {
        return cartData;
    }

    public void setCartDatamodel(CartDatamodel cartData) {
        this.cartData = cartData;
    }

}

