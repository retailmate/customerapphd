package com.cognizant.iot.homepage.models.productsearch;

/**
 * Created by 452781 on 2/22/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductSearchCompositionInfoModel {

    @SerializedName("VariantInformation")
    @Expose
    private ProductSearchVariantInfoModel variantInformation;
    @SerializedName("KitDefinition")
    @Expose
    private Object kitDefinition;

    public ProductSearchVariantInfoModel getVariantInformation() {
        return variantInformation;
    }

    public void setVariantInformation(ProductSearchVariantInfoModel variantInformation) {
        this.variantInformation = variantInformation;
    }

    public Object getKitDefinition() {
        return kitDefinition;
    }

    public void setKitDefinition(Object kitDefinition) {
        this.kitDefinition = kitDefinition;
    }

}