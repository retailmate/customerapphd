package com.cognizant.iot.homepage.models.productlist;

/**
 * Created by 452781 on 2/22/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductListValueModel {

    @SerializedName("ItemId")
    @Expose
    private String itemId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Price")
    @Expose
    private Double price;
    @SerializedName("PrimaryImageUrl")
    @Expose
    private String primaryImageUrl;
    @SerializedName("RecordId")
    @Expose
    private String recordId;
    @SerializedName("ExtensionProperties")
    @Expose
    private List<Object> extensionProperties = null;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getPrimaryImageUrl() {
        return primaryImageUrl;
    }

    public void setPrimaryImageUrl(String primaryImageUrl) {
        this.primaryImageUrl = primaryImageUrl;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public List<Object> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<Object> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

}