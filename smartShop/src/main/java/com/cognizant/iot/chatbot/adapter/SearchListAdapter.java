package com.cognizant.iot.chatbot.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.cognizant.iot.chatbot.model.SearchDataModel;
import com.cognizant.retailmate.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

//import com.squareup.picasso.Picasso;

/**
 * Created by 543898 on 10/18/2016.
 */
public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.ProductHolder> {
    private static final String CustomTag="CustomTag";

    private ArrayList<SearchDataModel> itemsList;
    private Context mContext;

    public SearchListAdapter(Context context, ArrayList<SearchDataModel> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chatbot_card_search, null);
        ProductHolder mh = new ProductHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, final int i) {

        SearchDataModel searchData = itemsList.get(i);
        holder.tvTitle.setText(searchData.getTitle());
        holder.priceView.setText(searchData.getSnippet()+"");
        Log.d("TAG","IMGurl for i = "+i+" is ="+searchData.getImgURL());


        Picasso.with(mContext)
                .load(searchData.getImgURL())
                .placeholder(R.drawable.chat_search_img_holder)
                .error(R.drawable.chat_search_img_holder)
                .into(holder.itemImage);
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent =new Intent(Intent.ACTION_VIEW, Uri.parse(itemsList.get(i).getLink()));
                mContext.startActivity(browserIntent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class ProductHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;

        protected ImageView itemImage;

        TextView priceView;

        LinearLayout linearLayout;




        public ProductHolder(final View view) {
            super(view);

            this.tvTitle = (TextView) view.findViewById(R.id.main_heading);
            this.itemImage = (ImageView) view.findViewById(R.id.searchImage);
            this.priceView= (TextView) view.findViewById(R.id.snippet_search);
            this.linearLayout= (LinearLayout) view.findViewById(R.id.Custom_search_card);

        }

    }

}