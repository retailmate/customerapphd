package com.cognizant.iot.chatbot.model;

/**
 * Created by 540472 on 2/10/2017.
 */
public class AnalyticsDataModel {


    /**
     * UserId : 004021
     * TimeStamp : Fri Feb 10 12:36:18 GMT+05:30 2017
     * Product_Name : Nike Roshe
     * Intent : isavailable
     */

    private String UserId;
    private String TimeStamp;
    private String Product_Name;
    private String Intent;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public String getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(String TimeStamp) {
        this.TimeStamp = TimeStamp;
    }

    public String getProduct_Name() {
        return Product_Name;
    }

    public void setProduct_Name(String Product_Name) {
        this.Product_Name = Product_Name;
    }

    public String getIntent() {
        return Intent;
    }

    public void setIntent(String Intent) {
        this.Intent = Intent;
    }
}
