package com.cognizant.iot.chatbot.model;

import java.util.ArrayList;

/**
 * Created by 543898 on 10/18/2016.
 */
public class ChatDataModel {
    String mDataset;
    String mTime;
    Integer mDatasetTypes;
    ArrayList<ProductDataModel> mProductArray;
    ArrayList<OfferDataModel> mOfferArray;
    ArrayList<SearchDataModel> mSearchArray;
    ArrayList<SuggestDataModel> mSuggestArray;


    public ArrayList<SuggestDataModel> getmSuggestArray() {
        return mSuggestArray;
    }

    public void setmSuggestArray(ArrayList<SuggestDataModel> mSuggestArray) {
        this.mSuggestArray = mSuggestArray;
    }

    public ArrayList<SearchDataModel> getmSearchArray() {
        return mSearchArray;
    }

    public void setmSearchArray(ArrayList<SearchDataModel> mSearchArray) {
        this.mSearchArray = mSearchArray;
    }

    public ArrayList<OfferDataModel> getmOfferArray() {
        return mOfferArray;
    }

    public void setmOfferArray(ArrayList<OfferDataModel> mOfferArray) {
        this.mOfferArray = mOfferArray;
    }

    public String getmDataset() {
        return mDataset;
    }

    public void setmDataset(String mDataset) {
        this.mDataset = mDataset;
    }

    public String getmTime() {
        return mTime;
    }

    public void setmTime(String mTime) {
        this.mTime = mTime;
    }

    public Integer getmDatasetTypes() {
        return mDatasetTypes;
    }

    public void setmDatasetTypes(Integer mDatasetTypes) {
        this.mDatasetTypes = mDatasetTypes;
    }


    public ArrayList<ProductDataModel> getmProductArray() {
        return mProductArray;
    }

    public void setmProductArray(ArrayList<ProductDataModel> mProductArray) {
        this.mProductArray = mProductArray;
    }


}
