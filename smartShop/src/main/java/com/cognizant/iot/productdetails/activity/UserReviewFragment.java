package com.cognizant.iot.productdetails.activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cognizant.iot.productdetails.adapter.ProductReviewRecyclerviewAdapter;
import com.cognizant.iot.productdetails.model.ReviewModel;
import com.cognizant.retailmate.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */

public class UserReviewFragment extends Fragment {

    //    private GridLayoutManager lLayout;
    private ProductReviewRecyclerviewAdapter productReviewRecyclerviewAdapter;

    List<ReviewModel> reviewList = new ArrayList<>();

    String type;

    public UserReviewFragment() {
        // Required empty public constructor
        this.type = "";
    }

    public UserReviewFragment(String type) {
        this.type = type;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_suggestedproduct, container, false);

        RecyclerView review = (RecyclerView) rootView.findViewById(R.id.recycler_view_1);
        TextView count = (TextView) rootView.findViewById(R.id.user_count);


        ReviewModel rmodel = new ReviewModel();

        rmodel = new ReviewModel();
        rmodel.setName("Mary Gibbs");
        rmodel.setDate("1/2/2017");
        rmodel.setReview("Awesome product at great price!");
        rmodel.setImage(getResources().getDrawable(R.drawable.designer_four));
        reviewList.add(rmodel);

        ReviewModel rmodel1 = new ReviewModel();
        rmodel1.setName("Jack Gibbs");
        rmodel1.setDate("12/12/2016");
        rmodel1.setReview("I love this product!");
        rmodel1.setImage(getResources().getDrawable(R.drawable.designer_one));
        reviewList.add(rmodel1);

        if (type.equals("users")) {

            ReviewModel rmodel2 = new ReviewModel();

            rmodel2 = new ReviewModel();
            rmodel2.setName("John Kerry");
            rmodel2.setDate("1/12/2016");
            rmodel2.setReview("Super!");
            rmodel2.setImage(getResources().getDrawable(R.drawable.designer_two));
            reviewList.add(rmodel2);

            ReviewModel rmodel3 = new ReviewModel();
            rmodel3.setName("Shaun Thomas");
            rmodel3.setDate("24/11/2016");
            rmodel3.setReview("Waiting eagerly for the shipment to arrive.");
            rmodel3.setImage(getResources().getDrawable(R.drawable.designer_three));
            reviewList.add(rmodel3);


        }

        count.setText(String.valueOf(reviewList.size()));


        productReviewRecyclerviewAdapter = new ProductReviewRecyclerviewAdapter(getContext(), reviewList);
        review.setAdapter(productReviewRecyclerviewAdapter);
        review.setNestedScrollingEnabled(false);
        review.setLayoutManager(new LinearLayoutManager(getActivity()));

        return rootView;
    }


}


