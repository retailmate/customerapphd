package com.cognizant.iot.mycart.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.bopis.RmBopis;
import com.cognizant.iot.homepage.ProductSearchAPI;
import com.cognizant.iot.homepage.models.cart.CartLineModel;
import com.cognizant.iot.homepage.models.wishlist.WishlistCommerceListLineModel;
import com.cognizant.iot.insidestore.RecommendedInStore;
import com.cognizant.iot.mycart.MyBagFragment;
import com.cognizant.iot.mycart.MyCart;
import com.cognizant.iot.orderhistory.activity.Order_Products;
import com.cognizant.iot.orderhistory.model.OrderModel;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by 452781 on 2/26/2017.
 */

public class BagRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    int lastPosition = -1;

    Context context;

    List<CartLineModel> cartList;
    TextView totalValue, totalCountItems, loyaltyPointsNumber;
    ImageView checkout;
    double value = 0;
    int totalCount = 0;
    String type;


    OrderModel[] orderModel;
    Double ordertotaltosend;
    OrderModel orderModelsingle;


    List<WishlistCommerceListLineModel> wishList;
    View wishlist_outofstockColorView;
    ImageView wishlist_outofstockrecommendation;

    public BagRecyclerAdapter(Context context, final List<WishlistCommerceListLineModel> commerceListLines) {
        this.context = context;
        this.wishList = commerceListLines;
        System.out.println("@@## wishlistLineModels count in adapter" + wishList.size());
        System.out.println("@@## wishList in adapter " + wishList.get(0).getProductSearchValueModel().getProductName());
        type = "wishlist";
    }

    public BagRecyclerAdapter(final Context context, OrderModel[] orderList) {
        this.context = context;
        this.orderModel = orderList;
        type = "orders";
    }


    public BagRecyclerAdapter(final Context context, final List<CartLineModel> cartList, TextView totalItemsCart, final TextView totalValueCart, ImageView checkoutCart, final TextView loyaltyPointsNumber) {
        this.cartList = cartList;
        this.context = context;
        type = "cart";
        checkout = checkoutCart;
        totalValue = totalValueCart;
        totalCountItems = totalItemsCart;

        checkoutCart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                AlertDialog.Builder alertbox = new AlertDialog.Builder(v
                        .getRootView().getContext(), R.style.MyDatePickerDialogTheme);
                alertbox.setMessage(context.getResources().getString(R.string.amountpayable) + "$"
                        + String.valueOf((Double.parseDouble(totalValueCart
                        .getText().toString()) - (Double
                        .parseDouble(loyaltyPointsNumber.getText()
                                .toString())))));
                alertbox.setTitle(R.string.cartvalue);
                alertbox.setIcon(R.drawable.cart_icon);
                alertbox.setNegativeButton(R.string.cancel, null);
                alertbox.setPositiveButton(R.string.continue_s,
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface arg0, int arg1) {
                                System.out.println("@@### CHECKOUT CLICKED : "
                                        + totalValueCart.getText().toString()
                                        + " payable ammount : $"
                                        + String.valueOf(String.format(
                                        "%.2f",
                                        (Double.parseDouble(totalValueCart
                                                .getText().toString()) - (Double
                                                .parseDouble(loyaltyPointsNumber
                                                        .getText()
                                                        .toString()))))));

                                Intent intent = new Intent(context, RmBopis.class);
                                intent.putExtra("productList", createCartProductListHashMap());
                                intent.putExtra("lpReedem", Double
                                        .parseDouble(loyaltyPointsNumber
                                                .getText().toString()));

                                context.startActivity(intent);
//                                ((Activity) context).finish();
                            }
                        });
                alertbox.show();
            }
        });

    }


    public class CartViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView prodid, prodcategorytv;
        TextView prodname;
        TextView prodprice;
        ImageView flag;
        TextView prodquantity;
        ImageView wishlist_cart;
        ImageView cartIcon_cart;

        public CartViewHolder(View view) {
            super(view);

            view.setOnClickListener(this);
            prodcategorytv = (TextView) view.findViewById(R.id.order_product_prodcategory_cart);
            prodname = (TextView) view.findViewById(R.id.order_product_cart);
            prodprice = (TextView) view.findViewById(R.id.order_product_prodprice1_cart);
            prodquantity = (TextView) view.findViewById(R.id.product_quantity_main1_cart);
            // Locate the ImageView in listview_item.xml
            flag = (ImageView) view.findViewById(R.id.image_order_products_cart);
            wishlist_cart = (ImageView) view.findViewById(R.id.wishlist_cart);
            cartIcon_cart = (ImageView) view.findViewById(R.id.cartIcon_cart);

            ImageView plus_cart = (ImageView) view.findViewById(R.id.plus_cart);
            ImageView minus_cart = (ImageView) view.findViewById(R.id.minus_cart);

            plus_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                prodquantity.setText(String.valueOf(Integer.parseInt((prodquantity.getText().toString())) + 1));
                    prodquantity.setText(String.valueOf(new Double((prodquantity.getText().toString())).intValue() + 1));

                    cartList.get(getAdapterPosition()).setQuantity((cartList.get(getAdapterPosition()).getQuantity().intValue() + 1));

                    notifyDataSetChanged();
                }
            });


            minus_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((new Double((prodquantity.getText().toString())).intValue()) > 1) {
//                    prodquantity.setText(String.valueOf(Integer.parseInt(prodquantity.getText().toString()) - 1));
                        prodquantity.setText(String.valueOf(new Double((prodquantity.getText().toString())).intValue() - 1));

                        cartList.get(getAdapterPosition()).setQuantity((cartList.get(getAdapterPosition()).getQuantity().intValue() - 1));
                        notifyDataSetChanged();
                    }
                }
            });

            cartIcon_cart.setOnClickListener(this);
            wishlist_cart.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {


            CartLineModel cartlineValue = new CartLineModel();
            int position = getAdapterPosition();
            cartlineValue = cartList.get(position);

            if (view.getId() == R.id.cartIcon_cart) {

                ProductSearchAPI productSearchAPI = new ProductSearchAPI(context, cartlineValue.getLineId());
                productSearchAPI.makecartdeleteCall();

                cartList.remove(position);
                notifyDataSetChanged();

            } else if (view.getId() == R.id.wishlist_cart) {

                if (R.drawable.hearticonnn == wishlist_cart.getId()) {
                    wishlist_cart.setImageResource(R.drawable.heart_grey);
                } else {
                    wishlist_cart.setImageResource(R.drawable.hearticonnn);
                }

                List<String> list = new ArrayList<String>();
                list.add(cartlineValue.getProductId());
                ProductSearchAPI productSearchAPI = new ProductSearchAPI(context, list, "wishlist");

                productSearchAPI.makeaCall();
            }


        }
    }

    public class OrdersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView orderid;
        TextView orderdate;
        TextView orderstatus;
        TextView orderprice;

        public OrdersViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);


            orderid = (TextView) itemView.findViewById(R.id.orderid_main);
            orderdate = (TextView) itemView.findViewById(R.id.order_date);
            orderstatus = (TextView) itemView.findViewById(R.id.order_status_main1);
            orderprice = (TextView) itemView.findViewById(R.id.order_price_main);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, Order_Products.class);

            intent.putExtra("OrderData", (Serializable) orderModelsingle);
            System.out.println("@@## Order Screen passing total" + ordertotaltosend);
            intent.putExtra("OrderPrice", ordertotaltosend);


            context.startActivity(intent);
        }

    }

    public class WishlistViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView category;
        TextView unit;
        TextView prodname;
        TextView prodprice, offer, offer_in_tag;
        ImageView flag;
        final ImageView delete_img_btn;
        final ImageView tag_cart_icon;
        ImageView tag;

        public WishlistViewHolder(View view) {
            super(view);

            view.setOnClickListener(this);
            category = (TextView) itemView.findViewById(R.id.prodcategory_wish);
            prodname = (TextView) itemView.findViewById(R.id.prodname_wish);
            prodprice = (TextView) itemView.findViewById(R.id.prodprice_wish1);
            delete_img_btn = (ImageView) itemView
                    .findViewById(R.id.delete_small_trash1);
            tag_cart_icon = (ImageView) itemView.findViewById(R.id.tag_cart_icon);
            // Locate the ImageView in listview_item.xml
            flag = (ImageView) itemView.findViewById(R.id.flag_wish);
            offer = (TextView) itemView.findViewById(R.id.offers_wish);
            offer_in_tag = (TextView) itemView.findViewById(R.id.offer_in_tag);
            unit = (TextView) itemView.findViewById(R.id.unit);
            tag = (ImageView) itemView.findViewById(R.id.tag);
            wishlist_outofstockColorView = itemView.findViewById(R.id.wishlist_outofstockColorView);
            wishlist_outofstockrecommendation = (ImageView) itemView.findViewById(R.id.wishlist_outofstockrecommendation);

            tag_cart_icon.setOnClickListener(this);
            delete_img_btn.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {


            WishlistCommerceListLineModel wishlistValue = new WishlistCommerceListLineModel();
            int position = getAdapterPosition();
            wishlistValue = wishList.get(position);

            if (view.getId() == R.id.tag_cart_icon) {

                if (R.drawable.cart_red_icon == tag_cart_icon.getId()) {
                    tag_cart_icon.setImageResource(R.drawable.cart_grey);
                } else {
                    tag_cart_icon.setImageResource(R.drawable.cart_red_icon);
                }

                List<String> list = new ArrayList<String>();
                list.add(wishlistValue.getProductId().toString());
                ProductSearchAPI productSearchAPI = new ProductSearchAPI(context, list, "cart");

                productSearchAPI.makeaCall();

            } else if (view.getId() == R.id.delete_small_trash1) {

                ProductSearchAPI productSearchAPI = new ProductSearchAPI(context, wishlistValue.getLineId());
                productSearchAPI.makeWishlistdelteCall();

                wishList.remove(position);
                notifyDataSetChanged();


            }
        }
    }

    @Override
    public int getItemViewType(int position) {


        if (type.equals("cart")) {
            return 0;
        } else if (type.equals("orders")) {
            return 1;
        } else {
            return 2;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case 1:
                View cartitemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.listview_item_order, parent, false);

                return new OrdersViewHolder(cartitemView);

            case 2:
                View wishlistitemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.listview_item_wish, parent, false);

                return new WishlistViewHolder(wishlistitemView);

            default:
                View cartitemView1 = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.cart_listview_item, parent, false);

                return new CartViewHolder(cartitemView1);

        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case 1:
                OrdersViewHolder ordersViewHolder = (OrdersViewHolder) holder;
                orderModelsingle = orderModel[position];

                double orderTotal = 0.0;

                ordersViewHolder.orderid.setText("INV-" + orderModelsingle.getSalesId());

//        System.out.println("@@## InvoiceId productData" + productData.get(0).get(Order_Screen.ORDERID));
//        orderdate.setText(productData.get(0).get(Order_Screen.ORDERDATE));
//        orderdate.setText(orderModelsingle.getDateCreated());


                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss");


                    Date newdate = sdf.parse(orderModelsingle.getDateCreated());

                    SimpleDateFormat sdftwo = new SimpleDateFormat("dd/MM/yyyy");
                    ordersViewHolder.orderdate.setText(sdftwo.format(newdate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                for (int i = 0; i < orderModelsingle.getItems().size(); i++) {
//            System.out.println("@@## ORDER PRICE INDIVIDUAL" + productData.get(i).get("prodprice"));
                    orderTotal = orderTotal + orderModelsingle.getItems().get(i).getLineAmount();
                }

                ordertotaltosend = orderTotal;

                ordersViewHolder.orderprice.setText("" + ((int) orderTotal));

                setAnimation(ordersViewHolder.itemView, position, "order");
                break;

            case 2:
                WishlistViewHolder wishlistViewHolder = (WishlistViewHolder) holder;
                WishlistCommerceListLineModel wishlineModel = wishList.get(position);

                try {
                    System.out.println("@@## wishlistLineModels position in adapter" + wishList.get(position).getProductSearchValueModel().getProductName() + "\n poisyion" + position);
                    wishlistViewHolder.category.setText(wishList.get(position).getProductSearchValueModel().getSearchName());
                    wishlistViewHolder.prodname.setText(wishList.get(position).getProductSearchValueModel().getProductName());
                    wishlistViewHolder.prodprice.setText(wishList.get(position).getProductSearchValueModel().getPrice().toString());

                    if (AccountState.getOfflineMode()) {
                        int image = context.getResources().getIdentifier(wishList.get(position).getProductSearchValueModel().getImage().getItems().get(0).getUrl(), "drawable", context.getPackageName());
                        Picasso.with(context).load(image).into(wishlistViewHolder.flag);
                    } else {
                        Picasso.with(context).load("https://landmarkdevret.cloudax.dynamics.com/MediaServer/" + wishList.get(position).getProductSearchValueModel().getImage().getItems().get(0).getUrl()).into(wishlistViewHolder.flag);
                    }
                    if (wishlineModel.getProductId().equals("68719483872") || wishlineModel.getProductId().equals("68719483125")) {
                        wishlist_outofstockColorView.setVisibility(View.VISIBLE);
                        wishlist_outofstockrecommendation.setVisibility(View.VISIBLE);

                        if (wishlineModel.getProductId().equals("68719483872")) {
                            wishlist_outofstockrecommendation.setTag("68719483872");
                        } else if (wishlineModel.getProductId().equals("68719483125")) {
                            wishlist_outofstockrecommendation.setTag("68719483125");
                        }
                    }

                    wishlist_outofstockrecommendation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String tag = v.getTag().toString();

                            if (tag.equals("68719483872")) {

                                new RecommendedInStore("68719483873", context).getPopUp(v, context);
                            }
                            if (tag.equals("68719483125")) {
                                new RecommendedInStore("68719483124", context).getPopUp(v, context);
                            }
                        }
                    });
                } catch (NullPointerException e) {
                }
                setAnimation(wishlistViewHolder.itemView, position, "wishlist");
                break;

            default:
                CartViewHolder cartViewHolder = (CartViewHolder) holder;
                CartLineModel model = cartList.get(position);

                try {
                    cartViewHolder.prodcategorytv.setText("Category : " + "Houseware");
                    cartViewHolder.prodname.setText(model.getProductSearchValueModel().getProductName());
                    cartViewHolder.prodprice.setText(String.format(Locale.US, "%.2f", model.getPrice()).toString());
                    totalValue.setText(String.format(Locale.US, "%.2f", Double.parseDouble(String.valueOf(calculateCartTotal()))).toString());

                    if (Double.parseDouble((String) MyBagFragment.loyaltyPointsNumber.getText()) > Double.parseDouble((String) MyBagFragment.totalValueCart.getText())) {
                        MyBagFragment.loyaltyPointsNumber.setText(MyBagFragment.totalValueCart.getText().toString());
                    }
                    totalCountItems.setText(String.valueOf(totalCount));
                    if (AccountState.getOfflineMode()) {
                        int image = context.getResources().getIdentifier(model.getProductSearchValueModel().getImage().getItems().get(0).getUrl(), "drawable", context.getPackageName());
                        Picasso.with(context).load(image).into(cartViewHolder.flag);
                    } else {
                        Picasso.with(context).load("https://landmarkdevret.cloudax.dynamics.com/MediaServer/" + model.getProductSearchValueModel().getImage().getItems().get(0).getUrl()).into(cartViewHolder.flag);
                    }
                } catch (NullPointerException e) {

                }
                setAnimation(cartViewHolder.itemView, position, "cart");
        }


    }

    @Override
    public int getItemCount() {
        if (type.equals("cart")) {
            return cartList.size();
        } else if (type.equals("orders")) {
            return orderModel.length;
        } else {
            return wishList.size();
        }
    }

    double calculateCartTotal() {
        value = 0;
        totalCount = 0;
        String valueString;
        String countString;
        for (int i = 0; i < cartList.size(); i++) {
            valueString = cartList.get(i).getPrice().toString();
            countString = cartList.get(i).getQuantity().toString();
            System.out.println("@@## valuestring " + valueString);
            // value = value + Integer.parseInt(valueString);
            value = value + (Double.parseDouble(valueString) * Double.parseDouble(countString));
            totalCount = totalCount + (int) Double.parseDouble(countString);
            MyCart.value1 = value;
        }

        return value;
    }

    ArrayList<HashMap<String, String>> createCartProductListHashMap() {
        ArrayList<HashMap<String, String>> data = new ArrayList<>();


        for (int i = 0; i < cartList.size(); i++) {
            HashMap<String, String> product = new HashMap<>();
            product.put("itemid", cartList.get(i).getItemId());
            product.put("productid", cartList.get(i).getProductId());
            product.put("quantity", cartList.get(i).getQuantity().toString());
            data.add(product);
        }
        return data;
    }


    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position, String type) {
        // If the bound view wasn't previously displayed on screen, it's animated

        if (position > lastPosition) {
            Animation animation;
            if (type.equals("cart")) {
                animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            } else if (type.equals("wishlist")) {
                animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
            } else {
                animation = AnimationUtils.loadAnimation(context, R.anim.slide_up);
            }
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}

