/*
package com.cognizant.iot.forgotpassword;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.retailmate.R;

public class ForgotPassword extends Activity {
	JSONObject jsonobject;
	JSONArray jsonarraynew, jsonarray;
	Button submit;
	EditText email;
	Boolean arrayempty = false;
	static String mail;
	static String Credentials;
	static String passresponse;
	int flag = 1;
	ArrayList<HashMap<String, String>> arraylist;
	static String emailval;
	static String imei = "000000000007777";
	String password;
	String currentTimeString;
	ProgressDialog mProgressDialog;
	final Context context = this;

	static String imeinum;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.passwordforgot);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayShowTitleEnabled(true);
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#0577D9")));
		getActionBar().setIcon(
				new ColorDrawable(getResources().getColor(
						android.R.color.transparent)));

		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");

		TextView abTitle = (TextView) findViewById(titleId);
		abTitle.setTextColor(Color.parseColor("#ffffff"));

		Intent myIntent = getIntent();
		imeinum = myIntent.getStringExtra("imei");
		email = (EditText) findViewById(R.id.forgot_email);// textbox to enter
															// mail id

		submit = (Button) findViewById(R.id.submit_forgot);// button for
															// submiting
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				emailval = email.getText().toString();
				new DownloadJSON().execute();

			}
		});

	}

	// DownloadJSON AsyncTask
	private class DownloadJSON extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(ForgotPassword.this);
			// Set progressdialog title
			mProgressDialog.setTitle("Forgot Password");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {

			jsonobject = JSONfunctin_forgot
					.getJSONfromURL(CatalogActivity.urlPart
							+ "/api/UserManagement/ForgetPassword");
			try {

				jsonarraynew = jsonobject.getJSONArray("cusdetails");

				passresponse = jsonarraynew.getString(0);
				if (passresponse
						.contains("Credential details are sent to registered Email")) {
					flag = 1;

				} else {
					flag = 0;
				}
				// toString();

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {

			if (flag == 1) {

				Toast.makeText(
						getApplicationContext(),
						"Credentials are sent to registered Email :" + emailval,
						80000).show();
			} else {
				Toast.makeText(getApplicationContext(),
						"Credentials are invalid", 80000).show();
			}
			mProgressDialog.dismiss();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// setMenuBackground();

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title

		// Handle action bar actions click
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
*/
